import express from 'express';
import { createServer } from 'node:http';
import { Server } from 'socket.io';

const app = express();
const server = createServer(app);
const io = new Server(server, {
    connectionStateRecovery: {},
    cors: {
        origin: "*"
    }
});


io.on('connection', async (socket) => {
    socket.on('new chat', (data) => {
        io.emit('new chat', {
            ...data,
            id: +new Date()
        });
    });
});

const port = process.env.PORT || 4444;

server.listen(port, () => {
    console.log(`server running at http://localhost:${port}`);
});